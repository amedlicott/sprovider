package com.digitalml.healthcare.identification.entities;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Provider:
{
  "type": "object",
  "properties": {
    "id": {
      "type": "string"
    },
    "effectiveDate": {
      "type": "string",
      "format": "date"
    },
    "terminationDate": {
      "type": "string",
      "format": "date"
    },
    "terminationReasonCode": {
      "$ref": "CodeDescription"
    },
    "person": {
      "$ref": "Person"
    },
    "totalProviderDiscountAmount": {
      "description": "TOTAL PROVIDER DISCOUNT AMOUNTT is a type of summed dollar amount that represents the sum of the discount savings amount is the value of the deal the company gets from a provider network in terms of savings on charges billed from a provider on a claim.",
      "$ref": "Amount"
    },
    "totalProviderPayPercent": {
      "type": "boolean"
    },
    "totalProviderResponsibilityAmount": {
      "description": "TOTAL PROVIDER RESPONSIBILITY AMOUNT  is a type of dollar amount that represents the calculated difference between the BILLED CHARGE AMOUNT and the ALLOWED AMOUNT at the claim level.",
      "$ref": "Amount"
    },
    "totalProviderWriteOffAmount": {
      "description": "TOTAL PROVIDER WRITE OFF AMOUNT this is the dollar amount that exceeds customary and reasonable charge or the provider discount for a particular claim.",
      "$ref": "Amount"
    }
  }
}
*/

public class Provider {

	@Size(max=1)
	private String id;

	@Size(max=1)
	private Date effectiveDate;

	@Size(max=1)
	private Date terminationDate;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.codeids.CodeDescription terminationReasonCode;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Person person;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount totalProviderDiscountAmount;

	@Size(max=1)
	private boolean totalProviderPayPercent;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount totalProviderResponsibilityAmount;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount totalProviderWriteOffAmount;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    id = null;
	    
	    
	    codeDescription = new com.digitalml.healthcare.foundation.codeids.CodeDescription();
	    person = new com.digitalml.healthcare.foundation.shared.Person();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	}
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	public Date getEffectiveDate() {
		return effectiveDate;
	}
	
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public Date getTerminationDate() {
		return terminationDate;
	}
	
	public void setTerminationDate(Date terminationDate) {
		this.terminationDate = terminationDate;
	}
	public com.digitalml.healthcare.foundation.codeids.CodeDescription getTerminationReasonCode() {
		return terminationReasonCode;
	}
	
	public void setTerminationReasonCode(com.digitalml.healthcare.foundation.codeids.CodeDescription terminationReasonCode) {
		this.terminationReasonCode = terminationReasonCode;
	}
	public com.digitalml.healthcare.foundation.shared.Person getPerson() {
		return person;
	}
	
	public void setPerson(com.digitalml.healthcare.foundation.shared.Person person) {
		this.person = person;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getTotalProviderDiscountAmount() {
		return totalProviderDiscountAmount;
	}
	
	public void setTotalProviderDiscountAmount(com.digitalml.healthcare.foundation.shared.Amount totalProviderDiscountAmount) {
		this.totalProviderDiscountAmount = totalProviderDiscountAmount;
	}
	public boolean getTotalProviderPayPercent() {
		return totalProviderPayPercent;
	}
	
	public void setTotalProviderPayPercent(boolean totalProviderPayPercent) {
		this.totalProviderPayPercent = totalProviderPayPercent;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getTotalProviderResponsibilityAmount() {
		return totalProviderResponsibilityAmount;
	}
	
	public void setTotalProviderResponsibilityAmount(com.digitalml.healthcare.foundation.shared.Amount totalProviderResponsibilityAmount) {
		this.totalProviderResponsibilityAmount = totalProviderResponsibilityAmount;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getTotalProviderWriteOffAmount() {
		return totalProviderWriteOffAmount;
	}
	
	public void setTotalProviderWriteOffAmount(com.digitalml.healthcare.foundation.shared.Amount totalProviderWriteOffAmount) {
		this.totalProviderWriteOffAmount = totalProviderWriteOffAmount;
	}
}