package com.digitalml.healthcare.foundation.codeids;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for CodeDescription:
{
  "type": "object",
  "properties": {
    "code": {
      "type": "string"
    },
    "name": {
      "type": "string"
    },
    "desc": {
      "type": "string"
    }
  }
}
*/

public class CodeDescription {

	@Size(max=1)
	private String code;

	@Size(max=1)
	private String name;

	@Size(max=1)
	private String desc;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    code = null;
	    name = null;
	    desc = null;
	}
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public String getDesc() {
		return desc;
	}
	
	public void setDesc(String desc) {
		this.desc = desc;
	}
}