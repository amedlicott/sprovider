package com.digitalml.rest.resources.codegentest.resource;
        	
import java.lang.IllegalArgumentException;
import java.security.AccessControlException;

import javax.servlet.http.HttpServletRequest;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import io.dropwizard.jersey.PATCH;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.lang.Object;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
	
// Customer specific imports

// Service specific imports
import com.digitalml.rest.resources.codegentest.service.SProviderService;
	
import com.digitalml.rest.resources.codegentest.service.SProviderService.GetASpecificProviderReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.SProviderService.GetASpecificProviderReturnDTO;
import com.digitalml.rest.resources.codegentest.service.SProviderService.GetASpecificProviderInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.SProviderService.FindProviderReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.SProviderService.FindProviderReturnDTO;
import com.digitalml.rest.resources.codegentest.service.SProviderService.FindProviderInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.SProviderService.UpdateProviderReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.SProviderService.UpdateProviderReturnDTO;
import com.digitalml.rest.resources.codegentest.service.SProviderService.UpdateProviderInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.SProviderService.ReplaceProviderReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.SProviderService.ReplaceProviderReturnDTO;
import com.digitalml.rest.resources.codegentest.service.SProviderService.ReplaceProviderInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.SProviderService.CreateProviderReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.SProviderService.CreateProviderReturnDTO;
import com.digitalml.rest.resources.codegentest.service.SProviderService.CreateProviderInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.SProviderService.DeleteProviderReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.SProviderService.DeleteProviderReturnDTO;
import com.digitalml.rest.resources.codegentest.service.SProviderService.DeleteProviderInputParametersDTO;
	
import com.digitalml.rest.resources.codegentest.*;
	
	/**
	 * Service: SProvider
	 * Manages Provider resources. A Provider is a Person or business that provides health care services to consumers. Most
health care providers are part of a network of some sort whether it is a HMO or PPO. These providers will typically see
all patients, but people not participating in the network will be charged more than those that belong to the network.
	 *
	 * @author admin
	 * @version 1.0
	 *
	 */
	
	@Path("http://digitalml.com/ICS/rest")
		@Produces({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
	public class SProviderResource {
		
	private static final Logger LOGGER = LoggerFactory.getLogger(SProviderResource.class);
	
	@Context
	private SecurityContext securityContext;

	@Context
	private javax.ws.rs.core.Request request;

	@Context
	private HttpServletRequest httpRequest;

	private SProviderService delegateService;

	private String implementationClass = "com.digitalml.rest.resources.codegentest.service.SProvider.SProviderServiceDefaultImpl";

	public void setImplementationClass(String className) {
		implementationClass = className;
	}

	public void setImplementationClass(Class clazz) {
		if (clazz == null)
			throw new IllegalArgumentException("Parameter clazz cannot be null");

		implementationClass = clazz.getName();
	}
		
	private SProviderService getCurrentImplementation() {

		Object object = null;
		try {
			Class c = Class.forName(implementationClass, true, Thread.currentThread().getContextClassLoader());
			object = c.newInstance();

		} catch (ClassNotFoundException exc) {
			LOGGER.error(implementationClass + " not found");
			return null;

		} catch (IllegalAccessException exc) {
			LOGGER.error("Cannot access " + implementationClass);
			return null;

		} catch (InstantiationException exc) {
			LOGGER.error("Cannot instantiate " + implementationClass);
			return null;
		}

		if (!(object instanceof SProviderService)) {
			LOGGER.error(implementationClass + " is not an instance of " + SProviderService.class.getName());
			return null;
		}

		return (SProviderService)object;
	}
	
	{
		delegateService = getCurrentImplementation();
	}
	
	public void setSecurityContext(SecurityContext securityContext) {
		this.securityContext = securityContext;
	}


	/**
	Method: getaspecificprovider
		Gets provider details

	Non-functional requirements:
	*/
	
	@GET
	@Path("/provider/{id}")
	public javax.ws.rs.core.Response getaspecificprovider(
		@PathParam("id")@NotEmpty String id) {

		GetASpecificProviderInputParametersDTO inputs = new SProviderService.GetASpecificProviderInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setId(id);
	
		try {
			GetASpecificProviderReturnDTO returnValue = delegateService.getaspecificprovider(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: findprovider
		Gets a collection of provider details filtered 

	Non-functional requirements:
	*/
	
	@GET
	@Path("/provider")
	public javax.ws.rs.core.Response findprovider(
		@QueryParam("offset") Integer offset,
		@QueryParam("pagesize") Integer pagesize) {

		FindProviderInputParametersDTO inputs = new SProviderService.FindProviderInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setOffset(offset);
		inputs.setPagesize(pagesize);
	
		try {
			FindProviderReturnDTO returnValue = delegateService.findprovider(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: updateprovider
		Updates provider

	Non-functional requirements:
	*/
	
	@PATCH
	@Path("/provider/{id}")
	public javax.ws.rs.core.Response updateprovider(
		 com.digitalml.rest.resources.codegentest.Request Request) {

		UpdateProviderInputParametersDTO inputs = new SProviderService.UpdateProviderInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setRequest(Request);
	
		try {
			UpdateProviderReturnDTO returnValue = delegateService.updateprovider(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: replaceprovider
		Replaces provider

	Non-functional requirements:
	*/
	
	@PUT
	@Path("/provider/{id}")
	public javax.ws.rs.core.Response replaceprovider(
		 com.digitalml.rest.resources.codegentest.Request Request) {

		ReplaceProviderInputParametersDTO inputs = new SProviderService.ReplaceProviderInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setRequest(Request);
	
		try {
			ReplaceProviderReturnDTO returnValue = delegateService.replaceprovider(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createprovider
		Creates provider

	Non-functional requirements:
	*/
	
	@POST
	@Path("/provider")
	public javax.ws.rs.core.Response createprovider(
		 com.digitalml.rest.resources.codegentest.Request Request) {

		CreateProviderInputParametersDTO inputs = new SProviderService.CreateProviderInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setRequest(Request);
	
		try {
			CreateProviderReturnDTO returnValue = delegateService.createprovider(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: deleteprovider
		Deletes provider

	Non-functional requirements:
	*/
	
	@DELETE
	@Path("/provider/{id}")
	public javax.ws.rs.core.Response deleteprovider(
		@PathParam("id")@NotEmpty String id) {

		DeleteProviderInputParametersDTO inputs = new SProviderService.DeleteProviderInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setId(id);
	
		try {
			DeleteProviderReturnDTO returnValue = delegateService.deleteprovider(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
}