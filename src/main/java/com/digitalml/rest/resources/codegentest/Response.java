package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Response:
{
  "required": [
    "provider"
  ],
  "type": "object",
  "properties": {
    "provider": {
      "$ref": "Provider"
    }
  }
}
*/

public class Response {

	@Size(max=1)
	@NotNull
	private com.digitalml.healthcare.identification.entities.Provider provider;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    provider = new com.digitalml.healthcare.identification.entities.Provider();
	}
	public com.digitalml.healthcare.identification.entities.Provider getProvider() {
		return provider;
	}
	
	public void setProvider(com.digitalml.healthcare.identification.entities.Provider provider) {
		this.provider = provider;
	}
}