package com.digitalml.rest.resources.codegentest.service.SProvider;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.Principal;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.ws.rs.core.SecurityContext;
import com.google.common.base.Strings;

import com.digitalml.rest.resources.codegentest.service.SProviderService;
	
/**
 * Default implementation for: SProvider
 * Manages Provider resources. A Provider is a Person or business that provides health care services to consumers. Most
health care providers are part of a network of some sort whether it is a HMO or PPO. These providers will typically see
all patients, but people not participating in the network will be charged more than those that belong to the network.
 *
 * @author admin
 * @version 1.0
 */

public class SProviderServiceDefaultImpl extends SProviderService {


    public GetASpecificProviderCurrentStateDTO getaspecificproviderUseCaseStep1(GetASpecificProviderCurrentStateDTO currentState) {
    

        GetASpecificProviderReturnStatusDTO returnStatus = new GetASpecificProviderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Manages Provider resources. A Provider is a Person or business that provides health care services to consumers. Most health care providers are part of a network of some sort whether it is a HMO or PPO. These providers will typically see all patients, but people not participating in the network will be charged more than those that belong to the network.");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public GetASpecificProviderCurrentStateDTO getaspecificproviderUseCaseStep2(GetASpecificProviderCurrentStateDTO currentState) {
    

        GetASpecificProviderReturnStatusDTO returnStatus = new GetASpecificProviderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Manages Provider resources. A Provider is a Person or business that provides health care services to consumers. Most health care providers are part of a network of some sort whether it is a HMO or PPO. These providers will typically see all patients, but people not participating in the network will be charged more than those that belong to the network.");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public GetASpecificProviderCurrentStateDTO getaspecificproviderUseCaseStep3(GetASpecificProviderCurrentStateDTO currentState) {
    

        GetASpecificProviderReturnStatusDTO returnStatus = new GetASpecificProviderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Manages Provider resources. A Provider is a Person or business that provides health care services to consumers. Most health care providers are part of a network of some sort whether it is a HMO or PPO. These providers will typically see all patients, but people not participating in the network will be charged more than those that belong to the network.");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public GetASpecificProviderCurrentStateDTO getaspecificproviderUseCaseStep4(GetASpecificProviderCurrentStateDTO currentState) {
    

        GetASpecificProviderReturnStatusDTO returnStatus = new GetASpecificProviderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Manages Provider resources. A Provider is a Person or business that provides health care services to consumers. Most health care providers are part of a network of some sort whether it is a HMO or PPO. These providers will typically see all patients, but people not participating in the network will be charged more than those that belong to the network.");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public GetASpecificProviderCurrentStateDTO getaspecificproviderUseCaseStep5(GetASpecificProviderCurrentStateDTO currentState) {
    

        GetASpecificProviderReturnStatusDTO returnStatus = new GetASpecificProviderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Manages Provider resources. A Provider is a Person or business that provides health care services to consumers. Most health care providers are part of a network of some sort whether it is a HMO or PPO. These providers will typically see all patients, but people not participating in the network will be charged more than those that belong to the network.");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public GetASpecificProviderCurrentStateDTO getaspecificproviderUseCaseStep6(GetASpecificProviderCurrentStateDTO currentState) {
    

        GetASpecificProviderReturnStatusDTO returnStatus = new GetASpecificProviderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Manages Provider resources. A Provider is a Person or business that provides health care services to consumers. Most health care providers are part of a network of some sort whether it is a HMO or PPO. These providers will typically see all patients, but people not participating in the network will be charged more than those that belong to the network.");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };


    public FindProviderCurrentStateDTO findproviderUseCaseStep1(FindProviderCurrentStateDTO currentState) {
    

        FindProviderReturnStatusDTO returnStatus = new FindProviderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Manages Provider resources. A Provider is a Person or business that provides health care services to consumers. Most health care providers are part of a network of some sort whether it is a HMO or PPO. These providers will typically see all patients, but people not participating in the network will be charged more than those that belong to the network.");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public FindProviderCurrentStateDTO findproviderUseCaseStep2(FindProviderCurrentStateDTO currentState) {
    

        FindProviderReturnStatusDTO returnStatus = new FindProviderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Manages Provider resources. A Provider is a Person or business that provides health care services to consumers. Most health care providers are part of a network of some sort whether it is a HMO or PPO. These providers will typically see all patients, but people not participating in the network will be charged more than those that belong to the network.");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public FindProviderCurrentStateDTO findproviderUseCaseStep3(FindProviderCurrentStateDTO currentState) {
    

        FindProviderReturnStatusDTO returnStatus = new FindProviderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Manages Provider resources. A Provider is a Person or business that provides health care services to consumers. Most health care providers are part of a network of some sort whether it is a HMO or PPO. These providers will typically see all patients, but people not participating in the network will be charged more than those that belong to the network.");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public FindProviderCurrentStateDTO findproviderUseCaseStep4(FindProviderCurrentStateDTO currentState) {
    

        FindProviderReturnStatusDTO returnStatus = new FindProviderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Manages Provider resources. A Provider is a Person or business that provides health care services to consumers. Most health care providers are part of a network of some sort whether it is a HMO or PPO. These providers will typically see all patients, but people not participating in the network will be charged more than those that belong to the network.");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public FindProviderCurrentStateDTO findproviderUseCaseStep5(FindProviderCurrentStateDTO currentState) {
    

        FindProviderReturnStatusDTO returnStatus = new FindProviderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Manages Provider resources. A Provider is a Person or business that provides health care services to consumers. Most health care providers are part of a network of some sort whether it is a HMO or PPO. These providers will typically see all patients, but people not participating in the network will be charged more than those that belong to the network.");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public FindProviderCurrentStateDTO findproviderUseCaseStep6(FindProviderCurrentStateDTO currentState) {
    

        FindProviderReturnStatusDTO returnStatus = new FindProviderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Manages Provider resources. A Provider is a Person or business that provides health care services to consumers. Most health care providers are part of a network of some sort whether it is a HMO or PPO. These providers will typically see all patients, but people not participating in the network will be charged more than those that belong to the network.");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };


    public UpdateProviderCurrentStateDTO updateproviderUseCaseStep1(UpdateProviderCurrentStateDTO currentState) {
    

        UpdateProviderReturnStatusDTO returnStatus = new UpdateProviderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Manages Provider resources. A Provider is a Person or business that provides health care services to consumers. Most health care providers are part of a network of some sort whether it is a HMO or PPO. These providers will typically see all patients, but people not participating in the network will be charged more than those that belong to the network.");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public UpdateProviderCurrentStateDTO updateproviderUseCaseStep2(UpdateProviderCurrentStateDTO currentState) {
    

        UpdateProviderReturnStatusDTO returnStatus = new UpdateProviderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Manages Provider resources. A Provider is a Person or business that provides health care services to consumers. Most health care providers are part of a network of some sort whether it is a HMO or PPO. These providers will typically see all patients, but people not participating in the network will be charged more than those that belong to the network.");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public UpdateProviderCurrentStateDTO updateproviderUseCaseStep3(UpdateProviderCurrentStateDTO currentState) {
    

        UpdateProviderReturnStatusDTO returnStatus = new UpdateProviderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Manages Provider resources. A Provider is a Person or business that provides health care services to consumers. Most health care providers are part of a network of some sort whether it is a HMO or PPO. These providers will typically see all patients, but people not participating in the network will be charged more than those that belong to the network.");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public UpdateProviderCurrentStateDTO updateproviderUseCaseStep4(UpdateProviderCurrentStateDTO currentState) {
    

        UpdateProviderReturnStatusDTO returnStatus = new UpdateProviderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Manages Provider resources. A Provider is a Person or business that provides health care services to consumers. Most health care providers are part of a network of some sort whether it is a HMO or PPO. These providers will typically see all patients, but people not participating in the network will be charged more than those that belong to the network.");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public UpdateProviderCurrentStateDTO updateproviderUseCaseStep5(UpdateProviderCurrentStateDTO currentState) {
    

        UpdateProviderReturnStatusDTO returnStatus = new UpdateProviderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Manages Provider resources. A Provider is a Person or business that provides health care services to consumers. Most health care providers are part of a network of some sort whether it is a HMO or PPO. These providers will typically see all patients, but people not participating in the network will be charged more than those that belong to the network.");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public UpdateProviderCurrentStateDTO updateproviderUseCaseStep6(UpdateProviderCurrentStateDTO currentState) {
    

        UpdateProviderReturnStatusDTO returnStatus = new UpdateProviderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Manages Provider resources. A Provider is a Person or business that provides health care services to consumers. Most health care providers are part of a network of some sort whether it is a HMO or PPO. These providers will typically see all patients, but people not participating in the network will be charged more than those that belong to the network.");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public UpdateProviderCurrentStateDTO updateproviderUseCaseStep7(UpdateProviderCurrentStateDTO currentState) {
    

        UpdateProviderReturnStatusDTO returnStatus = new UpdateProviderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Manages Provider resources. A Provider is a Person or business that provides health care services to consumers. Most health care providers are part of a network of some sort whether it is a HMO or PPO. These providers will typically see all patients, but people not participating in the network will be charged more than those that belong to the network.");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };


    public ReplaceProviderCurrentStateDTO replaceproviderUseCaseStep1(ReplaceProviderCurrentStateDTO currentState) {
    

        ReplaceProviderReturnStatusDTO returnStatus = new ReplaceProviderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Manages Provider resources. A Provider is a Person or business that provides health care services to consumers. Most health care providers are part of a network of some sort whether it is a HMO or PPO. These providers will typically see all patients, but people not participating in the network will be charged more than those that belong to the network.");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public ReplaceProviderCurrentStateDTO replaceproviderUseCaseStep2(ReplaceProviderCurrentStateDTO currentState) {
    

        ReplaceProviderReturnStatusDTO returnStatus = new ReplaceProviderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Manages Provider resources. A Provider is a Person or business that provides health care services to consumers. Most health care providers are part of a network of some sort whether it is a HMO or PPO. These providers will typically see all patients, but people not participating in the network will be charged more than those that belong to the network.");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public ReplaceProviderCurrentStateDTO replaceproviderUseCaseStep3(ReplaceProviderCurrentStateDTO currentState) {
    

        ReplaceProviderReturnStatusDTO returnStatus = new ReplaceProviderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Manages Provider resources. A Provider is a Person or business that provides health care services to consumers. Most health care providers are part of a network of some sort whether it is a HMO or PPO. These providers will typically see all patients, but people not participating in the network will be charged more than those that belong to the network.");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public ReplaceProviderCurrentStateDTO replaceproviderUseCaseStep4(ReplaceProviderCurrentStateDTO currentState) {
    

        ReplaceProviderReturnStatusDTO returnStatus = new ReplaceProviderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Manages Provider resources. A Provider is a Person or business that provides health care services to consumers. Most health care providers are part of a network of some sort whether it is a HMO or PPO. These providers will typically see all patients, but people not participating in the network will be charged more than those that belong to the network.");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public ReplaceProviderCurrentStateDTO replaceproviderUseCaseStep5(ReplaceProviderCurrentStateDTO currentState) {
    

        ReplaceProviderReturnStatusDTO returnStatus = new ReplaceProviderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Manages Provider resources. A Provider is a Person or business that provides health care services to consumers. Most health care providers are part of a network of some sort whether it is a HMO or PPO. These providers will typically see all patients, but people not participating in the network will be charged more than those that belong to the network.");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public ReplaceProviderCurrentStateDTO replaceproviderUseCaseStep6(ReplaceProviderCurrentStateDTO currentState) {
    

        ReplaceProviderReturnStatusDTO returnStatus = new ReplaceProviderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Manages Provider resources. A Provider is a Person or business that provides health care services to consumers. Most health care providers are part of a network of some sort whether it is a HMO or PPO. These providers will typically see all patients, but people not participating in the network will be charged more than those that belong to the network.");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public ReplaceProviderCurrentStateDTO replaceproviderUseCaseStep7(ReplaceProviderCurrentStateDTO currentState) {
    

        ReplaceProviderReturnStatusDTO returnStatus = new ReplaceProviderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Manages Provider resources. A Provider is a Person or business that provides health care services to consumers. Most health care providers are part of a network of some sort whether it is a HMO or PPO. These providers will typically see all patients, but people not participating in the network will be charged more than those that belong to the network.");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };


    public CreateProviderCurrentStateDTO createproviderUseCaseStep1(CreateProviderCurrentStateDTO currentState) {
    

        CreateProviderReturnStatusDTO returnStatus = new CreateProviderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Manages Provider resources. A Provider is a Person or business that provides health care services to consumers. Most health care providers are part of a network of some sort whether it is a HMO or PPO. These providers will typically see all patients, but people not participating in the network will be charged more than those that belong to the network.");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public CreateProviderCurrentStateDTO createproviderUseCaseStep2(CreateProviderCurrentStateDTO currentState) {
    

        CreateProviderReturnStatusDTO returnStatus = new CreateProviderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Manages Provider resources. A Provider is a Person or business that provides health care services to consumers. Most health care providers are part of a network of some sort whether it is a HMO or PPO. These providers will typically see all patients, but people not participating in the network will be charged more than those that belong to the network.");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public CreateProviderCurrentStateDTO createproviderUseCaseStep3(CreateProviderCurrentStateDTO currentState) {
    

        CreateProviderReturnStatusDTO returnStatus = new CreateProviderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Manages Provider resources. A Provider is a Person or business that provides health care services to consumers. Most health care providers are part of a network of some sort whether it is a HMO or PPO. These providers will typically see all patients, but people not participating in the network will be charged more than those that belong to the network.");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public CreateProviderCurrentStateDTO createproviderUseCaseStep4(CreateProviderCurrentStateDTO currentState) {
    

        CreateProviderReturnStatusDTO returnStatus = new CreateProviderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Manages Provider resources. A Provider is a Person or business that provides health care services to consumers. Most health care providers are part of a network of some sort whether it is a HMO or PPO. These providers will typically see all patients, but people not participating in the network will be charged more than those that belong to the network.");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public CreateProviderCurrentStateDTO createproviderUseCaseStep5(CreateProviderCurrentStateDTO currentState) {
    

        CreateProviderReturnStatusDTO returnStatus = new CreateProviderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Manages Provider resources. A Provider is a Person or business that provides health care services to consumers. Most health care providers are part of a network of some sort whether it is a HMO or PPO. These providers will typically see all patients, but people not participating in the network will be charged more than those that belong to the network.");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public CreateProviderCurrentStateDTO createproviderUseCaseStep6(CreateProviderCurrentStateDTO currentState) {
    

        CreateProviderReturnStatusDTO returnStatus = new CreateProviderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Manages Provider resources. A Provider is a Person or business that provides health care services to consumers. Most health care providers are part of a network of some sort whether it is a HMO or PPO. These providers will typically see all patients, but people not participating in the network will be charged more than those that belong to the network.");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };


    public DeleteProviderCurrentStateDTO deleteproviderUseCaseStep1(DeleteProviderCurrentStateDTO currentState) {
    

        DeleteProviderReturnStatusDTO returnStatus = new DeleteProviderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Manages Provider resources. A Provider is a Person or business that provides health care services to consumers. Most health care providers are part of a network of some sort whether it is a HMO or PPO. These providers will typically see all patients, but people not participating in the network will be charged more than those that belong to the network.");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public DeleteProviderCurrentStateDTO deleteproviderUseCaseStep2(DeleteProviderCurrentStateDTO currentState) {
    

        DeleteProviderReturnStatusDTO returnStatus = new DeleteProviderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Manages Provider resources. A Provider is a Person or business that provides health care services to consumers. Most health care providers are part of a network of some sort whether it is a HMO or PPO. These providers will typically see all patients, but people not participating in the network will be charged more than those that belong to the network.");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public DeleteProviderCurrentStateDTO deleteproviderUseCaseStep3(DeleteProviderCurrentStateDTO currentState) {
    

        DeleteProviderReturnStatusDTO returnStatus = new DeleteProviderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Manages Provider resources. A Provider is a Person or business that provides health care services to consumers. Most health care providers are part of a network of some sort whether it is a HMO or PPO. These providers will typically see all patients, but people not participating in the network will be charged more than those that belong to the network.");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public DeleteProviderCurrentStateDTO deleteproviderUseCaseStep4(DeleteProviderCurrentStateDTO currentState) {
    

        DeleteProviderReturnStatusDTO returnStatus = new DeleteProviderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Manages Provider resources. A Provider is a Person or business that provides health care services to consumers. Most health care providers are part of a network of some sort whether it is a HMO or PPO. These providers will typically see all patients, but people not participating in the network will be charged more than those that belong to the network.");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public DeleteProviderCurrentStateDTO deleteproviderUseCaseStep5(DeleteProviderCurrentStateDTO currentState) {
    

        DeleteProviderReturnStatusDTO returnStatus = new DeleteProviderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Manages Provider resources. A Provider is a Person or business that provides health care services to consumers. Most health care providers are part of a network of some sort whether it is a HMO or PPO. These providers will typically see all patients, but people not participating in the network will be charged more than those that belong to the network.");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

	/**
	 * Creates and returns a {@link Method} object using reflection and handles the possible exceptions.
	 * <br/>
	 * Aids with calling the process step method based on the outcome of the control logic
	 * 
	 * @param methodName
	 * @param clazz
	 * @return
	 */
	private Method getMethod(String methodName, Class clazz) {
		Method method = null;
		try {
			method = SProviderService.class.getMethod(methodName, clazz);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}

		return method;
	}
	
	/**
	 * For use when calling provider systems.
	 * <p>
	 * TODO: Implement security logic here
	 */
	protected SecurityContext securityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}