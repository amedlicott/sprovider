package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.SProvider.SProviderServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.SProviderService.ReplaceProviderInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.SProviderService.ReplaceProviderReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class ReplaceProviderTests {

	@Test
	public void testOperationReplaceProviderBasicMapping()  {
		SProviderServiceDefaultImpl serviceDefaultImpl = new SProviderServiceDefaultImpl();
		ReplaceProviderInputParametersDTO inputs = new ReplaceProviderInputParametersDTO();
		inputs.setRequest(new com.digitalml.rest.resources.codegentest.Request());
		ReplaceProviderReturnDTO returnValue = serviceDefaultImpl.replaceprovider(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}