package com.digitalml.rest.resources.codegentest.resource;

import java.security.Principal;

import org.apache.commons.collections.CollectionUtils;

import org.junit.Assert;
import org.junit.Test;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

public class SProviderTests {

	@Test
	public void testResourceInitialisation() {
		SProviderResource resource = new SProviderResource();
		Assert.assertNotNull(resource);
	}

	@Test
	public void testOperationGetASpecificProviderNoSecurity() {
		SProviderResource resource = new SProviderResource();
		resource.setSecurityContext(null);

		Response response = resource.getaspecificprovider(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationFindProviderNoSecurity() {
		SProviderResource resource = new SProviderResource();
		resource.setSecurityContext(null);

		Response response = resource.findprovider(0, 50);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationUpdateProviderNoSecurity() {
		SProviderResource resource = new SProviderResource();
		resource.setSecurityContext(null);

		Response response = resource.updateprovider(new com.digitalml.rest.resources.codegentest.Request());
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationReplaceProviderNoSecurity() {
		SProviderResource resource = new SProviderResource();
		resource.setSecurityContext(null);

		Response response = resource.replaceprovider(new com.digitalml.rest.resources.codegentest.Request());
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationCreateProviderNoSecurity() {
		SProviderResource resource = new SProviderResource();
		resource.setSecurityContext(null);

		Response response = resource.createprovider(new com.digitalml.rest.resources.codegentest.Request());
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationDeleteProviderNoSecurity() {
		SProviderResource resource = new SProviderResource();
		resource.setSecurityContext(null);

		Response response = resource.deleteprovider(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(403, response.getStatus());
	}


	private SecurityContext unautheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return false;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};

}