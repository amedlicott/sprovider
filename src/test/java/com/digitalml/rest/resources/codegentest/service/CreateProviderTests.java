package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.SProvider.SProviderServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.SProviderService.CreateProviderInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.SProviderService.CreateProviderReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class CreateProviderTests {

	@Test
	public void testOperationCreateProviderBasicMapping()  {
		SProviderServiceDefaultImpl serviceDefaultImpl = new SProviderServiceDefaultImpl();
		CreateProviderInputParametersDTO inputs = new CreateProviderInputParametersDTO();
		inputs.setRequest(new com.digitalml.rest.resources.codegentest.Request());
		CreateProviderReturnDTO returnValue = serviceDefaultImpl.createprovider(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponse());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}